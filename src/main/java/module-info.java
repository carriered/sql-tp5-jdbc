open module tp.oge {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;

    requires java.sql;

    requires org.hibernate.orm.core;
    requires jakarta.persistence;
    requires HibernateRepositories;

}
