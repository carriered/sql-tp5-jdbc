package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageRessourceDatabase implements Stockage<Ressource> {

    @Override
    public void create(Ressource element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "INSERT INTO RessourcesOGE (nom) VALUES (?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, element.getNom());
            statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ressource element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "UPDATE RessourcesOGE SET nom = (?) WHERE idRessource = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, element.getNom());
            statement.setInt(2, element.getIdRessource());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "DELETE FROM RessourcesOGE WHERE idRessource = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Ressource getById(int id) {
        Ressource ressource = null;
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "SELECT * FROM RessourcesOGE WHERE idRessource = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet result = statement.executeQuery())
            {
                result.next() ; //On accède à la prochaine ligne
                int idRessource = result.getInt( 1);
                String nom = result.getString( 2);
                ressource = new Ressource(nom);
                ressource.setIdRessource(idRessource);

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ressource;
    }

    @Override
    public List<Ressource> getAll() {
        List<Ressource> listeRessources = new ArrayList<>();
        int id; String nom;
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "SELECT * FROM RessourcesOGE";

        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(sql);
        )
        {
            while(result.next()) { //On accède à la prochaine ligne

                id = result.getInt(1);
                nom = result.getString(2);
                Ressource ressource = new Ressource(nom);
                ressource.setIdRessource(id);
                listeRessources.add(ressource);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeRessources;
    }
}
