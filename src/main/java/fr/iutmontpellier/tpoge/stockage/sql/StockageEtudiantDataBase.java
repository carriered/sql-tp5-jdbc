package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageEtudiantDataBase implements Stockage<Etudiant> {
    //EtudiantsOGE(idEtudiant, nom, prenom, idRessourceFavorite#)

    @Override
    public void create(Etudiant element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "INSERT INTO EtudiantsOGE (nom,prenom, idRessourceFavorite) VALUES (?,?,?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());
            statement.setInt(3, element.getRessourceFavorite().getIdRessource());

            statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Etudiant element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "UPDATE EtudiantsOGE SET nom = ?, prenom = ?, idRessourceFavorite = ? WHERE idEtudiant = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());
            statement.setInt(3, element.getRessourceFavorite().getIdRessource());
            statement.setInt(4, element.getIdEtudiant());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "DELETE FROM EtudiantsOGE WHERE idEtudiant = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Etudiant getById(int id) {
        Etudiant etudiant = null;
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "SELECT * FROM EtudiantsOGE WHERE idEtudiant = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet result = statement.executeQuery())
            {
                result.next() ; //On accède à la prochaine ligne
                int idEtudiant = result.getInt( 1);
                String nom = result.getString( 2);
                String prenom = result.getString( 3);
                int idRessourceFavorite = result.getInt( 4);
                etudiant = new Etudiant(nom, prenom,
                        new StockageRessourceDatabase().getById(idRessourceFavorite));

                etudiant.setIdEtudiant(idEtudiant);

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return etudiant;
    }

    @Override
    public List<Etudiant> getAll() {
        List<Etudiant> listeEtudiant = new ArrayList<>();
        int id; String nom; String prenom; int idRessourceFavorite;
        Connection connection = SQLUtils.getInstance().getConnection();
        String sql = "SELECT * FROM EtudiantsOGE";

        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(sql);
        )
        {
            while(result.next()) { //On accède à la prochaine ligne

                id = result.getInt(1);
                nom = result.getString(2);
                prenom = result.getString(3);
                idRessourceFavorite = result.getInt(4);
                Etudiant etudiant = new Etudiant(nom, prenom,
                        new StockageRessourceDatabase().getById(idRessourceFavorite));

                etudiant.setIdEtudiant(id);
                listeEtudiant.add(etudiant);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeEtudiant;
    }
}
